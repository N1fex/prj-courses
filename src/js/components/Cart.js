import DB from "@/js/components/DB";

export default class Cart{
  constructor({idCourse, count}){
    this.id = idCourse;
    this.count = count;

    if (!this.constructor.removeHandlerCart || !this.constructor.changeHandlerCart) {
      // вызовится один раз, при создании первого экземпляра, потом вызваться не будет, потому что УЖЕ в СТАТИЧЕСКУЮ переменную removeHandler добавится функция обработчик
      this.constructor.addRemoveHandlerCart();
      this.constructor.addChangeHandlerCart();
    }
  }

  static removeHandlerCart;
  static changeHandlerCart;

  static addRemoveHandlerCart() {
    this.removeHandlerCart = function (e) {
      if (e.target.matches('[data-delete-cart-id]')) {
        let data = DB.getDB();
        const id = Number(e.target.dataset.deleteCartId);

        data.cart = data.cart.filter(el => el.idCourse !== id)
        DB.setDB(data);
      }
    }

    document.addEventListener('click', this.removeHandlerCart)
  }

  static addChangeHandlerCart() {
    this.changeHandlerCart = function (e) {
      if (e.target.matches('[data-change-count-cart]')) {
        let data = DB.getDB();
        let direction = e.target.dataset.direction;
        const id = Number(e.target.dataset.changeCountCart);
        
        data.cart = data.cart.map(el => {
          if(el.idCourse === id && !(direction === 'down' && el.count === 1)){
            el.count = direction === 'up' ? ++el.count : --el.count
          }
          return el;
        })

        DB.setDB(data);
      }
    }

    document.addEventListener('click', this.changeHandlerCart)
  }

  static totalPrice(){
    let price = 0;
    let courses = DB.getDB().courses;

    DB.getDB().cart.map(elCart => {
      let course = courses.find(el => el.id === elCart.idCourse)
      price += course.price*elCart.count;
    })

    return new Intl.NumberFormat('ru-RU', {style: 'currency', currency: 'RUB'}).format(price)
  }


  render(){
    const course = DB.getDB().courses.filter(el => el.id === this.id)[0]

    return `
      <div class="cart-item">
        <div class="row mb-0">
          <div class="valign-wrapper">
            <div class="col s6 l5">
              <p class="cart-item_name">${course.title}</p>
            </div>
            <div class="col s6 l2">
              <p class="cart-item_price">${new Intl.NumberFormat('ru-RU', {style: 'currency', currency: 'RUB'}).format(course.price)}</p>
            </div>
            <div class="col s6 l2 cart-item_quantity">
              <div class="row mb-0 regulator">
                <a class="red lighten-3 btn-small" data-change-count-cart="${this.id}" data-direction="down">↓</a>
                <p>${this.count}</p>
                <a class="cyan darken-1 btn-small" data-change-count-cart="${this.id}" data-direction="up">↑</a>
              </div>
            </div>
            <div class="col s6 l2 center-align">
              <a class="red darken-1 btn-small" data-delete-cart-id="${this.id}">Удалить</a>
            </div>
          </div>
        </div>
      </div>
    `
  }
}