export default class DB{
  static initDB(){
    const DB = {
      courses: [],
      cart: []
    }

    this.setDB(DB)
  }

  static setDB(DB){
    localStorage.setItem('db', JSON.stringify(DB));
    // кастомное событие
    const event = new CustomEvent('DB:updated');
    document.dispatchEvent(event);
  }

  static getDB(){
    return JSON.parse(localStorage.getItem('db'));
  }
  
  static newCourse(course){
    let DB = this.getDB();

    DB.courses = [
      {id: +new Date, order: false, ...course},
      ...DB.courses
    ];
    this.setDB(DB)
  }
}